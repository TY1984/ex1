from Game_Features.vector import Vector
from Game_Features.ball import Ball


def test_move():
    vector1 = Vector(10, 10)
    vector1.move(Vector(5, 5))

    assert vector1.x == 15
    assert vector1.y == 15

    vector2 = Vector(15, 15)
    vector2.move(5)

    assert vector2.x == 20
    assert vector2.y == 20


def test_sub():
    vector1 = Vector(10, 10)
    vector2 = Vector(5, 5)
    vector3 = vector1 - vector2

    assert vector3.x == 5
    assert vector3.y == 5

    vector4 = Vector(10, 10)
    vector5 = vector4 - 7

    assert vector5.x == 3
    assert vector5.y == 3


def test_abs():
    vector1 = Vector(3, 4)

    assert vector1.__abs__() == 5


def test_ball_obj_creation():
    ball = Ball(30, 20, 150)

    assert ball.size == 20
    assert ball.speed == 30
    assert ball.vector.x == 199
    assert ball.vector.y == 150


def test_ball_movement():
    ball = Ball(30, 20, 150)
    vector1 = Vector(10, 10)
    ball.vector.move(vector1)

    assert ball.speed == 30
    assert ball.size == 20
    assert ball.vector.x == 209
    assert ball.vector.y == 160



