"""Flappy, game inspired by Flappy Bird.

Exercises

1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.

"""


from Game_Features.bird import Bird
from Games.game import Game
from turtle import setup, hideturtle, up, tracer, listen, onkey, ontimer, clear, goto, dot, update, done\
    , Turtle


if __name__ == '__main__':
    setup(420, 420, 1500, 0)
    bird = Bird(15, 0, 0)
    game = Game(bird)
    hideturtle()
    up()
    tracer(False)
    onkey(bird.up_arrow, "Up")
    onkey(bird.left_arrow, "Left")
    onkey(bird.right_arrow, "Right")
    listen()
    game.move()
    done()
