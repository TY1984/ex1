from Game_Features.shape import Shape


class Ball(Shape):
    """
    In order to be able to control each and every ball I had to build a class Ball and assign its variables
    upon ball creation
    """
    def __init__(self, speed, size, y):
        super().__init__(size, 199, y)
        self.speed = speed

