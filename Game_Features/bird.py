from Game_Features.vector import Vector
from Game_Features.shape import Shape


class Bird(Shape):
    def __init__(self, size, x, y):
        super().__init__(size, x, y)
        self.score = 0

    def up_arrow(self):
        """Up key will cause the bird to "jump" 30 in y axis and add 1 point to the score"""
        up = Vector(0, 30)
        self.vector.move(up)
        self.score += 1

    def left_arrow(self):
        """Left key will cause the bird to "jump" -10 backwards in x axis and reduce 1 point from the score"""
        left = Vector(-10, 0)
        self.vector.move(left)
        self.score -= 1

    def right_arrow(self):
        """Right key will cause the bird to "jump" 10 forward in x axis and add 10 point to the score"""
        right = Vector(10, 0)
        self.vector.move(right)
        self.score += 10
