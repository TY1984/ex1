from abc import ABC
from Game_Features.vector import Vector


class Shape(ABC):
    def __init__(self, size, x, y):
        if size <= 0:
            raise ValueError
        self.vector = Vector(x, y)
        self.size = size
