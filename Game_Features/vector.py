class Vector:
    """
    Class Vector comes to make sure we can perform mathematical operations on Vectors
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self, vector_to_manip):
        """
        moving a vector object in a given coordinates / number
        """
        if isinstance(vector_to_manip, Vector):
            self.x += vector_to_manip.x
            self.y += vector_to_manip.y
        else:
            self.x += vector_to_manip
            self.y += vector_to_manip

    def __sub__(self, vector_to_manip):
        """
        creating a new vector with values subtracted from the given vector / number
        """
        if isinstance(vector_to_manip, Vector):
            return Vector(self.x - vector_to_manip.x, self.y - vector_to_manip.y)
        else:
            return Vector(self.x - vector_to_manip, self.y - vector_to_manip)

    def __abs__(self):
        """
        calculate the distance between a bird and a ball
        """
        return (self.x ** 2 + self.y ** 2) ** 0.5

