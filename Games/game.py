from random import randint, randrange
from turtle import setup, hideturtle, up, tracer, listen, onkey, ontimer, clear, goto, dot, update, done\
    , Turtle
from Game_Features.ball import Ball


class Game:
    def __init__(self, bird):
        self.bird = bird
        self.writer = Turtle(visible=False)
        self.balls = []

    def inside(self, point):
        """Return True if point on screen."""
        return -200 < point.x < 200 and -200 < point.y < 200

    def draw(self, alive):
        """Draw screen objects."""
        clear()
        goto(self.bird.vector.x, self.bird.vector.y)
        if alive:
            self.writer.undo()
            dot(10, 'green')
            self.writer.color("Orange")
            self.writer.write("Score: {} ".format(self.bird.score))
        else:
            self.writer.undo()
            dot(10, 'red')
            self.writer.color("red")
            self.writer.write("Game over with: {} ".format(self.bird.score))
            for ball in self.balls:
                ball.size = 0
        for ball in self.balls:
            goto(ball.vector.x, ball.vector.y)
            dot(ball.size, 'black')
        update()

    def move(self):
        """Update object positions."""
        self.bird.vector.y -= 5  # gravity speed
        for ball in self.balls:
            ball.vector.x -= ball.speed  # control ball speed
        if randrange(10) == 0:  # Adding a ball to the screen as soon as rand=0
            y = randrange(-199, 199)
            size = randint(10, 50)
            speed = randint(3, 10)
            ball = Ball(speed, size, y)
            self.balls.append(ball)
        while len(self.balls) > 0 and not self.inside(self.balls[0].vector):  # pops a ball from the array(reached left side of the screen
            self.balls.pop(0)
        if not self.inside(self.bird.vector):
            self.draw(False)
            return
        for ball in self.balls:
            if abs(ball.vector - self.bird.vector) < (5 + ball.size / 2):
                self.draw(False)
                return
        self.draw(True)
        ontimer(self.move, 50)
